import Users from "./material/Users";
import Categories from "./material/Categories";
import Articles from "./material/Articles";
import { Switch, Route } from "react-router-dom";
import { Drawer, Button, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Divider } from "@mui/material";
import { useState } from "react";
import PeopleAltIcon from "@mui/icons-material/PeopleAlt";
import FolderOpenIcon from "@mui/icons-material/FolderOpen";
import FileCopyIcon from "@mui/icons-material/FileCopy";
import CssBaseline from "@mui/material/CssBaseline";
import { useHistory } from "react-router-dom";

function App() {
    const [open, setOpen] = useState(false);
    const history = useHistory();

    function changePath(path) {
        history.push(path);
        setOpen(false);
    }

    return (
        <>
            <CssBaseline />
            <Drawer anchor="left" open={open} onClose={() => setOpen(false)}>
                <List>
                    <ListItem>
                        <ListItemButton onClick={() => changePath("/users")}>
                            <ListItemIcon>
                                <PeopleAltIcon />
                            </ListItemIcon>
                            <ListItemText primary="Хэрэглэгч" />
                        </ListItemButton>
                    </ListItem>

                    <ListItem>
                        <ListItemButton onClick={() => changePath("/categories")}>
                            <ListItemIcon>
                                <FolderOpenIcon />
                            </ListItemIcon>
                            <ListItemText primary="Ангилал" />
                        </ListItemButton>
                    </ListItem>
                    <ListItem>
                        <ListItemButton onClick={() => changePath("/articles")}>
                            <ListItemIcon>
                                <FileCopyIcon />
                            </ListItemIcon>
                            <ListItemText primary="Нийтлэл" />
                        </ListItemButton>
                    </ListItem>
                </List>
            </Drawer>

            <Button variant="contained" onClick={() => setOpen(true)}>
                Цэс
            </Button>

            <Switch>
                <Route path="/users">
                    <Users />
                </Route>
                <Route path="/articles">
                    <Articles />
                </Route>
                <Route path="/categories">
                    <Categories />
                </Route>
            </Switch>
        </>
    );
}

export default App;
