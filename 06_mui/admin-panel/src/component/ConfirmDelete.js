export default function ConfirmDelete() {
    return (
        <div className="modal is-active">
            <div className="modal-background"></div>
            <div className="modal-content" style={{ maxWidth: 400 }}>
                <div className="box">
                    <h2 className="title is-3 has-text-centered">Устгах уу?</h2>

                    <div className="buttons">
                        <button className="button" style={{ flex: 1 }}>
                            Үгүй
                        </button>
                        <button className="button is-danger" style={{ flex: 1 }}>
                            Устга
                        </button>
                    </div>
                </div>
            </div>
            <button className="modal-close is-large" aria-label="close"></button>
        </div>
    );
}
